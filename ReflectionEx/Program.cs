﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace ReflectionEx
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentAssembly = Assembly.GetExecutingAssembly();
            Console.WriteLine("Name = " + currentAssembly.GetName().Name);
            Console.WriteLine("Full Name = " + currentAssembly.FullName);
            Console.WriteLine("Location = " + currentAssembly.Location);
            Console.ReadKey();

        }
    }
}
